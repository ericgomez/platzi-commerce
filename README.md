# Curso de Introducción a Vue.js 3
#### Proyecto del curso

![Proyect](https://static.platzi.com/media/landing-projects/proyecto-vue3.png)

## Platzi Commerce

![Proyect2](https://static.platzi.com/media/user_upload/Proyect2-d1b97efe-a5a3-417d-8aff-3cb35558420d.jpg)

Crea tu propio eCommerce con Vue.js. 
Descubre cómo desarrollar un framework reactivo con Proxies y Reflects para comprender mucho mejor cómo construyeron Vue.js 3 One Piece


## License
[MIT](https://choosealicense.com/licenses/mit/)
